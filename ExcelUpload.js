import { useState } from 'react';
import * as XLSX from 'xlsx';

const ExcelUpload = () => {

    const [file,setFile] = useState(null);
    const [data,setData] = useState([]);

    const handleFileChange = (e) => {
        const selectedFile = e.target.files[0];

        const reader = new FileReader();

        reader.onload = (e) => {
            const fileData = e.target.result;
            const workbook = XLSX.read(fileData,{type:'binary'});
            const sheetName = workbook.SheetNames[0];

            const sheetData = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]);

            setData(sheetData);
        }

        reader.readAsBinaryString(selectedFile);
    }

    console.log(data);

    return (
        <div>
            <input type='file' accept='.xls,.xlsx' onChange={handleFileChange}/>

            {data.length > 0 && (
                <div>
                    <h2>Preview: </h2>
                    <table>
                        <thead>
                            <tr>
                                {Object.keys(data[0]).map((header,index) => (
                                    <th key = {index}>{header}</th>
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((row,rowIndex) => (
                                <tr key={rowIndex}>
                                    {Object.values(row).map((cell,cellIndex) => (
                                        <td key={cellIndex}>{cell}</td>
                            ))}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            )

            }
        </div>
    )
}

export default ExcelUpload;